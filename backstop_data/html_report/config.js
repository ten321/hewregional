report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWebRegionalsTest_A11y_Summit_Home_Page_0_document_0_phone.png",
        "test": "../bitmaps_test/20230527-113148/BackstopHighEdWebRegionalsTest_A11y_Summit_Home_Page_0_document_0_phone.png",
        "selector": "document",
        "fileName": "BackstopHighEdWebRegionalsTest_A11y_Summit_Home_Page_0_document_0_phone.png",
        "label": "A11y Summit Home Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewregional.pantheonsite.io/accessibilitysummit/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWebRegionalsTest_A11y_Summit_Home_Page_0_document_1_tablet.png",
        "test": "../bitmaps_test/20230527-113148/BackstopHighEdWebRegionalsTest_A11y_Summit_Home_Page_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "BackstopHighEdWebRegionalsTest_A11y_Summit_Home_Page_0_document_1_tablet.png",
        "label": "A11y Summit Home Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewregional.pantheonsite.io/accessibilitysummit/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWebRegionalsTest_A11y_Summit_Home_Page_0_document_2_desktop.png",
        "test": "../bitmaps_test/20230527-113148/BackstopHighEdWebRegionalsTest_A11y_Summit_Home_Page_0_document_2_desktop.png",
        "selector": "document",
        "fileName": "BackstopHighEdWebRegionalsTest_A11y_Summit_Home_Page_0_document_2_desktop.png",
        "label": "A11y Summit Home Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewregional.pantheonsite.io/accessibilitysummit/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWebRegionalsTest_HighEdWeb_2021_Annual_Conference_Home_Page_0_document_0_phone.png",
        "test": "../bitmaps_test/20230527-113148/BackstopHighEdWebRegionalsTest_HighEdWeb_2021_Annual_Conference_Home_Page_0_document_0_phone.png",
        "selector": "document",
        "fileName": "BackstopHighEdWebRegionalsTest_HighEdWeb_2021_Annual_Conference_Home_Page_0_document_0_phone.png",
        "label": "HighEdWeb 2021 Annual Conference Home Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewregional.pantheonsite.io/2021/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWebRegionalsTest_HighEdWeb_2021_Annual_Conference_Home_Page_0_document_1_tablet.png",
        "test": "../bitmaps_test/20230527-113148/BackstopHighEdWebRegionalsTest_HighEdWeb_2021_Annual_Conference_Home_Page_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "BackstopHighEdWebRegionalsTest_HighEdWeb_2021_Annual_Conference_Home_Page_0_document_1_tablet.png",
        "label": "HighEdWeb 2021 Annual Conference Home Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewregional.pantheonsite.io/2021/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWebRegionalsTest_HighEdWeb_2021_Annual_Conference_Home_Page_0_document_2_desktop.png",
        "test": "../bitmaps_test/20230527-113148/BackstopHighEdWebRegionalsTest_HighEdWeb_2021_Annual_Conference_Home_Page_0_document_2_desktop.png",
        "selector": "document",
        "fileName": "BackstopHighEdWebRegionalsTest_HighEdWeb_2021_Annual_Conference_Home_Page_0_document_2_desktop.png",
        "label": "HighEdWeb 2021 Annual Conference Home Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewregional.pantheonsite.io/2021/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWebRegionalsTest_A11y_Summit_2021_Home_Page_0_document_0_phone.png",
        "test": "../bitmaps_test/20230527-113148/BackstopHighEdWebRegionalsTest_A11y_Summit_2021_Home_Page_0_document_0_phone.png",
        "selector": "document",
        "fileName": "BackstopHighEdWebRegionalsTest_A11y_Summit_2021_Home_Page_0_document_0_phone.png",
        "label": "A11y Summit 2021 Home Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewregional.pantheonsite.io/accessibilitysummit20/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWebRegionalsTest_A11y_Summit_2021_Home_Page_0_document_1_tablet.png",
        "test": "../bitmaps_test/20230527-113148/BackstopHighEdWebRegionalsTest_A11y_Summit_2021_Home_Page_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "BackstopHighEdWebRegionalsTest_A11y_Summit_2021_Home_Page_0_document_1_tablet.png",
        "label": "A11y Summit 2021 Home Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewregional.pantheonsite.io/accessibilitysummit20/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWebRegionalsTest_A11y_Summit_2021_Home_Page_0_document_2_desktop.png",
        "test": "../bitmaps_test/20230527-113148/BackstopHighEdWebRegionalsTest_A11y_Summit_2021_Home_Page_0_document_2_desktop.png",
        "selector": "document",
        "fileName": "BackstopHighEdWebRegionalsTest_A11y_Summit_2021_Home_Page_0_document_2_desktop.png",
        "label": "A11y Summit 2021 Home Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewregional.pantheonsite.io/accessibilitysummit20/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWebRegionalsTest_A11y_Summmit_2020_Replays_0_document_0_phone.png",
        "test": "../bitmaps_test/20230527-113148/BackstopHighEdWebRegionalsTest_A11y_Summmit_2020_Replays_0_document_0_phone.png",
        "selector": "document",
        "fileName": "BackstopHighEdWebRegionalsTest_A11y_Summmit_2020_Replays_0_document_0_phone.png",
        "label": "A11y Summmit 2020 Replays",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewregional.pantheonsite.io/accessibilitysummit20/replays/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWebRegionalsTest_A11y_Summmit_2020_Replays_0_document_1_tablet.png",
        "test": "../bitmaps_test/20230527-113148/BackstopHighEdWebRegionalsTest_A11y_Summmit_2020_Replays_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "BackstopHighEdWebRegionalsTest_A11y_Summmit_2020_Replays_0_document_1_tablet.png",
        "label": "A11y Summmit 2020 Replays",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewregional.pantheonsite.io/accessibilitysummit20/replays/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/BackstopHighEdWebRegionalsTest_A11y_Summmit_2020_Replays_0_document_2_desktop.png",
        "test": "../bitmaps_test/20230527-113148/BackstopHighEdWebRegionalsTest_A11y_Summmit_2020_Replays_0_document_2_desktop.png",
        "selector": "document",
        "fileName": "BackstopHighEdWebRegionalsTest_A11y_Summmit_2020_Replays_0_document_2_desktop.png",
        "label": "A11y Summmit 2020 Replays",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewregional.pantheonsite.io/accessibilitysummit20/replays/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    }
  ],
  "id": "Backstop HighEdWeb Regionals Test"
});